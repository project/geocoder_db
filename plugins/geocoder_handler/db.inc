<?php
/**
 * @file
 * Plugin to provide a database geocoder.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t("DB Geocoder"),
  'description' => t('Geocodes via custom database fields'),
  'callback' => 'geocoder_db_geocode',
  'field_types' => array(
    'text',
    'text_long',
    'addressfield',
    'location',
    'text_with_summary',
    'computed',
    'taxonomy_term_reference'
  ),
  'field_callback' => 'geocoder_db_geocode_field',
);

/**
 * Callback to geocode address via db
 */
function geocoder_db_geocode($address, $options = array()) {
  geophp_load();

  $db_settings = variable_get('geocoder_db', array());
  $db_settings = array_filter($db_settings);

  if (isset($db_settings['table']) && isset($db_settings['condition'])) {
    $query = db_select($db_settings['table'], 't');
    $query->fields('t', array($db_settings['lat'], $db_settings['lon']));
    $query->range(0, 1);

    $conditions = explode(',', $db_settings['condition']);
    if (count($conditions) > 1) {
      $or = db_or();
      foreach ($conditions as $condition) {
        $or->where('LOWER(' . $condition . ') LIKE BINARY :address', array(
          ':address' => $address,
        ));
      }
      $query->condition($or);
    }
    else {
      $query->where('LOWER(' . $conditions[0] . ') LIKE BINARY :address', array(
        ':address' => $address,
      ));
    }

    $row = $query->execute()->fetchAssoc();

    if ($row) {
      return new Point($row[$db_settings['lon']], $row[$db_settings['lat']]);
    }
  }

  if (isset($db_settings['fallback'])) {
    return geocoder($db_settings['fallback'], $address, $options);
  }

  return FALSE;
}

function geocoder_db_geocode_field($field, $field_item, $options = array()) {
  if ($field['type'] == 'text' || $field['type'] == 'text_long' || $field['type'] == 'text_with_summary' || $field['type'] == 'computed') {
    return geocoder_db_geocode($field_item['value'], $options);
  }
  if ($field['type'] == 'addressfield') {
    $address = geocoder_widget_parse_addressfield($field_item);
    return geocoder_db_geocode($address, $options);
  }
  if ($field['type'] == 'location') {
    $address = geocoder_widget_parse_locationfield($field_item);
    return geocoder_db_geocode($address, $options);
  }
  if ($field['type'] == 'taxonomy_term_reference') {
    $term = taxonomy_term_load($field_item['tid']);
    return geocoder_db_geocode($term->name);
  }
}